<?php
function customExceptionHandler() {
    echo 'Error';
    http_response_code(409); //Client error status code -> Conflict
    exit();
}
set_exception_handler('customExceptionHandler');

if($_SERVER['REQUEST_METHOD'] != 'POST' || $_SERVER['CONTENT_TYPE'] != 'application/json'){
    header('Location: index.html', true, 302);
    exit();
}

require("fpdf.php");

$json = file_get_contents("php://input");
$data = json_decode($json, true);

$invoiceName = filter_var($data["name"], FILTER_SANITIZE_STRING);
$invoiceDate = filter_var($data["timestamp"], FILTER_SANITIZE_NUMBER_INT);
$invoiceTax = filter_var($data["tax"], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
$invoiceProducts = $data["products"];

$invoiceTaxAvailables = array(10.5, 21, 27);

assert(strlen($invoiceName) > 0 && strlen($invoiceName) <= 24);
assert($invoiceDate > 1577847600 && $invoiceDate < 2000000000); //2020-2033
assert(in_array($invoiceTax, $invoiceTaxAvailables));
assert(count($invoiceProducts) > 0 && count($invoiceProducts) <= 20);

$invoiceDateReadable = date('d/m/Y', $invoiceDate);

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 16);

$pdf->Cell(0, 10, 'Factura', 0, 1, 'C');
$pdf->Ln(5);

$pdf->SetFont('Arial', '', 12);

$pdf->Cell(50, 5, 'Cliente:', 0, 0);
$pdf->Cell(0, 5, $invoiceName, 0, 1);

$pdf->Cell(50, 5, 'Fecha de factura:', 0, 0);
$pdf->Cell(0, 5, $invoiceDateReadable, 0, 1);

$pdf->Cell(50, 5, 'Impuesto IVA:', 0, 0);
$pdf->Cell(0, 5, $invoiceTax . "%", 0, 1);

$pdf->Ln(2);
$pdf->Cell(0, 5, '', 'T', 1);

$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(0, 10, 'Detalle: ', 0, 1, 'L');

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(20, 6, 'Codigo', 1, 0, 'C');
$pdf->Cell(80, 6, 'Descripcion', 1, 0, 'C');
$pdf->Cell(35, 6, 'Precio unitario', 1, 0, 'C');
$pdf->Cell(25, 6, 'Cantidad', 1, 0, 'C');
$pdf->Cell(35, 6, 'Total', 1, 1, 'C');

$pdf->SetFont('Arial', '', 12);

$productsSubTotalPrice = 0;

foreach ($invoiceProducts as $productId => $productData) {
    $productId = filter_var($productId, FILTER_SANITIZE_NUMBER_INT);
    $productDescription = filter_var($productData["description"], FILTER_SANITIZE_STRING);
    $productPrice = filter_var($productData["price"], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $productQuantity = filter_var($productData["quantity"], FILTER_SANITIZE_NUMBER_INT);

    assert($productId > 0 && $productId <= 999);
    assert(strlen($productDescription) > 0 && strlen($productDescription) <= 20);
    assert($productPrice > 0 && $productPrice < 99999999);
    assert($productQuantity > 0 && $productQuantity < 9999);

    $totalPrice = $productPrice * $productQuantity;
    $productsSubTotalPrice += $totalPrice;

    $pdf->Cell(20, 6, $productId, 1, 0, 'C');
    $pdf->Cell(80, 6, $productDescription, 1, 0, 'C');
    $pdf->Cell(35, 6, "$" . $productPrice, 1, 0, 'C');
    $pdf->Cell(25, 6, $productQuantity, 1, 0, 'C');
    $pdf->Cell(35, 6, "$" . $totalPrice, 1, 1, 'C');
}

$productsTotalTax = $productsSubTotalPrice * ($invoiceTax / 100);
$productsTotalPrice = $productsSubTotalPrice + $productsTotalTax;

$pdf->SetFont('Arial', '', 14);
$pdf->Ln(4);

$pdf->Cell(0, 5, 'Subtotal:', 0, 0);
$pdf->Cell(0, 5, '$' . number_format($productsSubTotalPrice, 2), 0, 1, 'R');

$pdf->Cell(0, 5, 'Impuestos:', 0, 0);
$pdf->Cell(0, 5, '$' . number_format($productsTotalTax, 2), 0, 1, 'R');

$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(0, 5, 'Total:', 0, 0);
$pdf->Cell(0, 5, '$' . number_format($productsTotalPrice, 2), 0, 1, 'R');

$pdf->Ln(12);

$pdf->SetFont('Arial', 'I', 10);
$pdf->Cell(0, 10, 'Trabajo Practico I | CDS UNSO 2023', 0, 0, 'C');

$pdf->Output();

?>