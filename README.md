# Facturador

Project developed in the CDS subject (Software Development Concepts) of the UNSO, commission 3.

## Requirements:

* PHP >= 7 (you can use xampp/lampp)
* FPDF library (included in the repository)

## Security

Within the backend, all exceptions are handled to avoid disclosure of information, in case of not complying with any of the policies, the error code 409 (Customer conflict) will be returned.

Each input data is sanitized depending on the type of value, in addition a previous verification is carried out in the frontend for a better user experience.

### Policies

| Name                | Type of data           | Range of data value   |
| ------------------- | ---------------------- | --------------------- |
| invoice name        | String                 | 0 < strlen <= 24      |
| invoice date        | Timestamp (integer)    | 2020 < date < 2033    |
| invoice tax         | Float                  | 10.5  \| 21 \| 27     |
| invoice products    | Array                  | 0 < size <= 20        |
| product id          | Integer                | 0 < value <= 999      |
| product description | String                 | 0 < strlen <= 20      |
| product price       | Integer                | 0 < value <= 99999999 |
| product quantity    | Integer                | 0 < value <= 9999     |

## TODO

* Improve the design of the invoice pdf
